// Auth Server
var crypto = require("crypto");
var {BigInteger} = require("jsbn");
var net = require("net");
const {_undef, _nudef, _def} = require("./varsenal");
var {decodeRealmPacket, encodeRealmPacket} = require("./networking");

var test_database = {
    "VALE":{
        salt:"EA7C2A124EC1F01D678604ECA41272E7ECE233D3481349F57CC14E4033E015BF", // s
        verifier:"1EFCBC3D5936BE21750792E058D0FCFB93394E451D5C89A8E92A7C22FA719186" // v
    }
};

/**
 * Client:                             Server:
 p = params["2048"]                  p = params["2048"]
 s1 = genKey()                       s2 = genKey()
 c = new Client(p,salt,id,pw,s1)     s = new Server(p,verifier,s2)
 A = c.computeA()            A---->  s.setA(A)
 c.setB(B)                <-----B    B = s.computeB()
 M1 = c.computeM1()         M1---->  s.checkM1(M1) // may throw error
 K = c.computeK()                    K = s.computeK()
 */

let SRP = {
    N:new BigInteger("894B645E89E1535BBDAD5B8B290650530801B18EBFBF5E8FAB3C82872A3E9BB7", 16),
    g:new BigInteger("07", 16)

}

function hashCredentials(username, password){
    let u=username.upper(), p = password.upper();
    let shasum = crypto.createHash("sha1");
    shasum.update(u);
    shasum.update(":");
    shasum.update(p);
    return shasum.digest("hex");
}

function createSaltAndVerifier(password_hash){
    let salt = crypto.randomBytes(32);
    let shasum = crypto.createHash("sha1");
    sha.update(salt);
    sha.update(password_hash);
    let x = new BigInteger(shasum.digest("hex"), 16);
    let verifier = SRP.g.modPow(x, SRP.N);
    return {salt,verifier};
}

function calculateB(verifier){
    let b = new BigInteger(crypto.randomBytes(20).toString("hex"), 16);
    let gmod = SRP.g.modPow(b, SRP.N);
    let B = verifier.multiply(new BigInteger("3")).add(gmod).mod(SRP.N);
}

function calculateM2(A, verifier){
    let shasum = crypto.createHash("sha1");
    let B = calculateB(verifier);
    shasum.update()
}

// let SRP = {
//     N:null, // hex2bn 894B645E89E1535BBDAD5B8B290650530801B18EBFBF5E8FAB3C82872A3E9BB7
//     s:null, // setrand 19*8
//     g:null, // set (d)word 7
//     v:null, // verifier
//     b:null, 
//     B:null,
//     K:null
// }

var realm = net.createServer(function createAuthServer(socket){
    socket.on("data", function receiveClientAuthData(data){
        console.debug("Heard from socket:", data);
        var packet = decodeRealmPacket(data);
        console.debug("Deciphering as realm packet:", packet);
        if(packet._COMMAND=="AuthLogonChallenge"){
            let {account_name} = packet;
            console.debug("Recognizing login challenge.");
            console.debug("Looking for account: ", account_name);

            // TODO: Later will be DB and must prevent sql injection
            if(_def(test_database[account_name])){
                console.debug("Found account.");
                console.debug("Generating and sending back ephemeral.");
                let {secret, public} = srp.generateEphemeral(test_database[account_name].verifier);
                let server_params = {
                    B:public,
                    g:srp_params.g.toHex(),
                    n:srp_params.N.toHex(),
                    srp_salt:test_database[account_name].salt
                };
                console.debug("Server params: ", server_params);
                let AuthLogonChallenge_Server = encodeRealmPacket("AuthLogonChallenge", server_params);
                console.debug("Server response: ", AuthLogonChallenge_Server);
                socket.write(AuthLogonChallenge_Server, function doneWriting(){
                    console.debug("Wrote to client.");
                });
            }
        }
    })
});

realm.on("connection", function new_auth_conn(socket){
    console.log("Receivied new client.");
});

realm.listen(3724, function auth_ready(){
    console.log("We ready.");
});