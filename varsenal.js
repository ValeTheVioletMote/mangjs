// V_Arsenal

function _undef(questionable){
    return (typeof questionable == 'undefined');
}

function _nudef(questionable){
    return _undef(questionable) || questionable == null;
}

function _def(questionable){
    return !_nudef(questionable);
}

module.exports = {_undef, _nudef, _def};