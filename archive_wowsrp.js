// Most of this is taken from https://github.com/azerothcore/ac-nodejs-srp6/blob/main/index.js
const crypto = require("crypto");

/**@typedef {{N_length_bits: number, N: BigInt, g: BigInt, hash: string}} SRPParams */

/**@type{Record<string,SRPParams>} */
const SRP_PARAMS = {
    wotlk: {
        N_length_bits: 256,
        N: BigInt("0x894B645E89E1535BBDAD5B8B290650530801B18EBFBF5E8FAB3C82872A3E9BB7"),
        g: BigInt("0x7"),
        hash: "sha1",
        k: BigInt("0x3")
    }
}






/**
 * 
 * @param {Buffer} salt 
 * @param {string} identity 
 * @param {string} password
 * @param {SRPParams} params 
 * compute the intermediate value x as a hash of three buffers:
 * salt, identity, and password.  And a colon.  FOUR buffers.
 *
 *      x = LE(H(s | H(I | ":" | P)))
 *
 * params:
 *         salt     (buffer)    salt
 *         identity (string)    user identity
 *         password (string)    user password
 *         LE                   little endian
 *
 * returns: x (bignum)      user secret
 * @returns {bigint}
 */
function getX( salt, identity, password, params=SRP_PARAMS.wotlk ) {
    const hash_ip = crypto.createHash( params.hash )
        .update( identity + ":" + password )
        .digest();
    const hash_X = crypto.createHash( params.hash )
        .update( salt )
        .update( hash_ip )
        .digest();
    return hash_X.readBigInt64LE();
}


/**
 * 
 * @param {Buffer} salt 
 * @param {string} identity 
 * @param {string} password 
 * @param {SRPParams} params
 * @returns {Buffer}
 * 
 * The verifier is calculated as described in Section 3 of [SRP-RFC].
 * We give the algorithm here for convenience.
 *
 * The verifier (v) is little endian computed based on the salt (s), user name (I),
 * password (P), and group parameters (N, g).
 *
 *         x = LE(H(s | H(user | ":" | pass)))
 *         v = LE(g^x % N)
 *
 * params:
 *         params (obj)     group parameters, with .N, .g, .hash
 *         salt (buffer)        salt
 *         identity (string)    user identity
 *         identity (string)    user password
 *         LE                   little endian
 *
 */
function compute_verifier( salt, identity, password, params=SRP_PARAMS.wotlk ) {
    const X = getX( salt, identity, password, params );
    const verifier = modularPowerBigInt(params.g, X, params.N)
    const verifier_le = verifier.toString(16).match(/.{2}/g).reverse().join("")
    return Buffer.from(verifier_le, "hex");
}


function generate_server_credentials( )
{
    
    // b 
}


module.exports = {
    compute_verifier
}