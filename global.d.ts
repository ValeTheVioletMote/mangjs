type LoginRequest = {
    c: "LOGIN_REQUEST",
    error: number,
    size: number,
    game: string,
    version: string,
    os: string,
    locale: string,
    utc_offset_minutes: number,
    ip: number,
    account: string
}