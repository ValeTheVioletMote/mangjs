var {StringDecoder} = require("string_decoder")
const {_undef, _nudef, _def} = require("./varsenal");

var RealmCommunications = [
    {
        name:"AuthLogonChallenge",
        command:"0",
        client:{
            "0":{
                name:"command",
                type:"uint8"
            },
            "1":{
                name:"error",
                type:"uint8"
            },
            "2":{ // Length of package minus 4
                name:"size",
                type:"uint16"
            },
            "4":{
                name:"gamename",
                type:"char",
                len:4
            },
            "8":{
                name:"version",
                type:"uint8",
                count:3,
                join:"."
            },
            "11":{
                name:"build",
                type:"uint16"
            },
            "13":{
                name:"platform",
                type:"char",
                len:4,
                reverse:true
            },
            "17":{
                name:"os",
                type:"char",
                len:4,
                reverse:true
            },
            "21":{
                name:"country",
                type:"char",
                len:4,
                reverse:true,
            },
            "25":{
                name:"worldregion_bias", // Time in minutes off from UTC
                type:"int32"
            },
            "29":{
                name:"ip",
                type:"uint8",
                count:4,
                join:"."
            },
            "33":{
                name:"account_name_len",
                type:"uint8"
            },
            "34":{
                name:"account_name",
                type:"char",
                len:"account_name_len"
            }
        },
        server:[
            // {
            //     type:"uint8",
            //     value:0
            // },
            {
                name:"command",
                type:"uint8",
                value:0
            },
            {
                name:"err",
                type:"uint8",
                value:0
            },
            {
                name:"unk",
                type:"uint8",
                value:0
            },
            {
                name:"B", // SRP public ephemeral
                type:"char",
                len:"32",
                from:"hex"
            },
            {
                name:"g_len", // SRP generator length
                type:"uint8",
                value:1
            },
            {
                name:"g", // SRP generator
                type:"uint8",
                from:"hex"
            },
            // {
            //     name:"n_len", // SRP modulus length
            //     type:"uint8"
            // },
            {
                name:"n", // SRP modulus
                type:"char",
                len:"variable", // Will auto-append n_len
                from:"hex"
            },
            {
                name:"srp_salt", // SRP user's salt
                type:"char"
            },
            // {
            //     name:"crc_salt", // Unclear... 
            //     type:"char"
            // }
        ]
    }
];

function decodeRealmPacket(uint8_arr_data){
    // Anticipate command always as the first byte.
    let command = uint8_arr_data[0];
    let comm = RealmCommunications.find(function findMatchingComm(c){
        return c.command == command;
    });
    if(comm == undefined){
        throw new Error("Unknown command: "+command);
    }
    let decoded = {};
    let str_decoder = new StringDecoder("utf8");
    let data_view = new DataView(uint8_arr_data.buffer);
    for(let [ind, info] of Object.entries(comm.client)){
        let index = Number(ind);
        let resp = null;
        let reversed = _def(info.reverse) && info.reverse==true;
        switch(info.type){
            case "uint8":
                if(_def(info.count)){
                    resp = Array.from(uint8_arr_data.slice(index, index+info.count));
                    if(_def(info.join)){
                        resp = resp.join(info.join);
                    }
                }else{
                    resp = uint8_arr_data[index];
                }
                break;
            case "char":
                let length = info.len;
                if(isNaN(length)){
                    length = decoded[info.len]; // Check for variable value.
                }
                resp = str_decoder.write(uint8_arr_data.slice(index, index+length));
                if(reversed){
                    resp = resp.split("").reverse().join("");
                }
                break;
            case "uint16":
                resp = data_view.getUint16(index, !reversed); // Little endian
                break;
            case "uint32":
                resp = data_view.getUint32(index, !reversed);
                break;
            case "int16":
                resp = data_view.getInt16(index, !reversed);
                break;
            case "int32":
                resp = data_view.getInt32(index, !reversed);
                break;
            default:
                break;
        }
        decoded[info.name] = resp;
    }

    decoded._COMMAND = comm.name;

    return decoded;
}

function encodeRealmPacket(name, data){
    let sendPacketBlueprint = RealmCommunications.find(function getRightComm(comm){
        return comm.name==name;
    });
    if(_undef(sendPacketBlueprint)){
        throw new Error("Could not find communication by name of "+name);
    }
    let response = [];
    
    for(let section of sendPacketBlueprint.server){
        let val = data[section.name] || section.value || 0;
        let vals = [];

        //Can use on node: Uint8Array.from(buffer)

        switch(section.type){
            case "uint8":
                if(section.from=="hex"){
                    vals.push(...Buffer.from(val, 'hex'));
                }else{
                    vals.push(Number(val));
                }
                break;
            case "char":
                vals.push(...Buffer.from(val, section.from));
                break;
            case "uint32":
            case "int32":
                let arb = new ArrayBuffer(4);
                let dv = new DataView(arb);
                if(section.type == "uint32"){
                    dv.setUint32(0, val, false);
                }else{
                    dv.setInt32(0, val, false);
                }
                vals.push(...arb);
                break;
        }

        if(_def(section.len) && section.len=="variable"){ // Prepend length when required
            response.push(vals.length);
        }

        response.push(...vals);
    }

    let buffy = Buffer.from(response);
    return buffy;
}

module.exports = {decodeRealmPacket, encodeRealmPacket, RealmCommunications};