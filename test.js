var NET = require("net");

// var realm = NET.createServer(function createAuthServer(socket){
//     // socket.on("data", function receiveClientAuthData(data){
//     //     console.debug("Heard from socket:", data);
//     // })
// });

var realm = new NET.Server()

realm.on("connection", function new_auth_conn(socket){
    console.log("Received new client.");
    socket.on("data", function(data) {

        console.log("heard from socket")
        // console.log(data.toJSON())



        const cmd = data.readUInt8();
        if(cmd == 0x0) {
            console.log("AUTH_LOGON_CHALLENGE");

            /**@type{LoginRequest} */
            const info = {
                c: "CLIENT_LOGON_CHALLENGE", 
                error : data.readUInt8(1),
                remainder_size : data.readUInt16LE(2),
                game : read_LEC_string(data, 4, 3), // 4? \x00 keeps showing up on 4
                version_major : data.readUInt8(8),
                version_minor : data.readUInt8(9),
                version_patch : data.readUInt8(10),
                version_build : data.readUInt16LE(11),
                os_arch : read_LEC_string(data, 13, 3), // 4? \x00 keeps showing up on 4
                os_os : read_LEC_string(data, 17, 3), // 4?  \x00 keeps showing up on 4   -> best guess is that this is the kind where you read up to the NULL (which I assume is \x00)
                locale : read_LEC_string(data, 21, 4),
                utc_offset_minutes : data.readIntLE(25, 4),
                ip : data.readIntLE(29, 4),
                account_name_length : data.readUInt8(33)
            }

            info.os = info.os_os+"_"+info.os_arch;
            delete info.os_os; delete info.os_arch;
            info.version = info.version_major+"."+info.version_minor+"."+info.version_patch+"."+info.version_build;
            delete info.version_major; delete info.version_minor; delete info.version_patch; delete info.version_build;

            info.account = data.slice(34, 34+info.account_name_length).toString("ascii"); // Not LE
            delete info.account_name_length;

            // const 
            console.log(info)

            const valesalt = Buffer.from(["F0", "7E", "BB", "5F", "61", "F7", "57", "F6"
            , "77", "95", "FA", "D2", "6A", "E4", "67", "84"
            , "B4", "B3", "A6", "20", "1D", "8C", "64", "FA"
            , "7A", "34", "2C", "70", "D5", "A1", "8D", "DB"]);


            const VERSION_CHALLENGE = Buffer.from(
                ["0xBA", "0xA3", "0x1E", "0x99", "0xA0", "0x0B"
			    ,   "0x21", "0x57", "0xFC", "0x37", "0x3F", "0xB3", "0x69", "0xCD", "0xD2", "0xF1"]

            )

            const v = calculate_password_verifier( info.account, "ADMA10R3", valesalt )
            const b = crypto.randomBytes(32);
            const B = calculate_server_public_key( v, b );

            const response_arr = [];
            response_arr.push("0x0", "0x0");
            response_arr.push("0x0") // WOW_SUCCESS  https://github.com/ProjectSkyfire/SkyFire_548/blob/main/src/server/authserver/Authentication/AuthCodes.h
            response_arr.push( ...B )
            response_arr.push("0x1")
            response_arr.push("0x7")
            response_arr.push("32")
            response_arr.push( ...LargeSafePrimeLEB )
            response_arr.push( ...valesalt )
            response_arr.push( ...VERSION_CHALLENGE )
            response_arr.push( "0x0" )

            // probably an issue with padding

            /**
             * 
             * 
             * 
             * Type	Size (bytes)	Reason
            Session Key	40	Two SHA-1 hashes appended are always 40 bytes.
            Server and Client Proof	20	Result of SHA-1 hash is always 20 bytes.
            Public Key	32	Packet field is always 32 bytes.
            Private Key	32	Arbitrarily chosen.
            Salt	32	Packet field is always 32 bytes.
            Password Verifier	32	Result of modulus large safe prime is always 32 bytes or less.
            Large Safe Prime	32	Largest value client will accept. Any larger can result in public key not fitting into packet field.
            S Key	32	Result of modulus large safe prime is always 32 bytes or less.
            Reconnect Seed Data	16	Packet field is always 16 bytes for both client and server.
            Generator	1	There is no reason to have a generator value above 255.
             */

            const response = Buffer.from(response_arr).reverse();
            console.log("Responding with:")
            console.log(response);

            socket.write( response );



        }


    })
});

realm.listen(3725, function auth_ready(){
    console.log("We ready.");
});

realm.on("close", () => console.log("A connection was closed"))
realm.on("listening", () => console.log("A connection is listening"))
realm.on("error", () => console.log("A connection was errored"))


/**
 * 
 * @param {Buffer} buff 
 * @param {number} start 
 * @param {number} length 
 */
function read_LEC_string(buff, start, length)
{
    return buff.slice(start, start+length).reverse().toString("ascii");
}

/**
 * 
 * @param {Buffer} buff 
 * @param {number} start 
 * @param {number} length 
 */
function read_BEC_string(buff, start, length)
{
    return buff.slice(start, start+length).toString("ascii");
}



const {compute_verifier} = require("./archive_wowsrp");
const crypto = require("crypto");
const { calculate_server_public_key, calculate_password_verifier, LargeSafePrimeLEB } = require("./wowsrp");

/**
 * 
 * @param {LoginRequest} lr 
 */
function receive_login_request(lr) {
    const salt = crypto.randomBytes(32);
    const v = compute_verifier( salt, lr.account, "");

    // store verifier and salt?



}


/**
 * 
 * Account: a
 * Password: b
 {
  type: 'Buffer',
  data: [
      0,   8,  31,  0, 87, 111,  87,   0,   3,
      3,   5,  52, 48, 54,  56, 120,   0, 110,
    105,  87,   0, 83, 85, 110, 101, 212, 254,
    255, 255, 127,  0,  0,   1,   1,  65
  ]
}


    Account: g
    Password: o

{
  type: 'Buffer',
  data: [
      0,   8,  31,  0, 87, 111,  87,   0,   3,
      3,   5,  52, 48, 54,  56, 120,   0, 110,
    105,  87,   0, 83, 85, 110, 101, 212, 254,
    255, 255, 127,  0,  0,   1,   1,  71
  ]
}



    Account: hi
    Password: hello

{
  type: 'Buffer',
  data: [
      0,   8,  32,  0, 87, 111,  87,   0,   3,
      3,   5,  52, 48, 54,  56, 120,   0, 110,
    105,  87,   0, 83, 85, 110, 101, 212, 254,
    255, 255, 127,  0,  0,   1,   2,  72,  73
  ]
}


    Account: hi
    Password: beans

{
  type: 'Buffer',
  data: [
      0,   8,  32,  0, 87, 111,  87,   0,   3,
      3,   5,  52, 48, 54,  56, 120,   0, 110,
    105,  87,   0, 83, 85, 110, 101, 212, 254,
    255, 255, 127,  0,  0,   1,   2,  72,  73
  ]
}


    So first call is just about the account name


 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 */