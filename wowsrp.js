const crypto = require("crypto");

const SRP_PARAMS = {
    N_length_bits: 256,
}


// N  - big endian string
const LargeSafePrime = BigInt("0x894B645E89E1535BBDAD5B8B290650530801B18EBFBF5E8FAB3C82872A3E9BB7");
const LargeSafePrimeLEB = bigint_to_lebuffer( LargeSafePrime );

// g
const Generator =  BigInt("0x7");

const HashType = "sha1";

// k
const KValue = BigInt("0x3");

// X
const XOR_HASH = calculate_xor_hash( Generator, LargeSafePrime );


// Used https://gtker.com/implementation-guide-for-the-world-of-warcraft-flavor-of-srp6/#important-knowledge-before-starting
// as a guide


// Stolen from https://github.com/Richienb/modular-power/blob/main/index.js
// /**
//  * 
//  * @param {bigint} base 
//  * @param {bigint} exponent 
//  * @param {bigint} modulus 
//  * @returns 
//  */
//  function modularPowerBigInt(base, exponent, modulus) {

// 	if (modulus === 1n) {
// 		return 0n;
// 	}

// 	base %= modulus;

// 	let result = 1n;

// 	while (exponent > 0n) {
// 		if (exponent % 2n === 1n) {
// 			result = (result * base) % modulus;
// 		}

// 		exponent >>= 1n; // eslint-disable-line no-bitwise
// 		base = (base ** 2n) % modulus;
// 	}

// 	return result;
// }



// bigint_ functions stolen from https://github.com/juanelas/bigint-mod-arith/


/**
 * Finds the smallest positive element that is congruent to a in modulo n
 * @param {bigint} a
 * @param {bigint} n
 * @returns {bigint} A bigint with the smallest positive representation of a modulo n 
 */
function bigint_toZn( a, n ) {
    const aZn = a % n;
    return ( aZn < 0n ) ? aZn + n : aZn

}

/**
 * 
 * @param {bigint} a
 * @returns {bigint} The absolute value of a
 */
function bigint_abs( a ) {
    return ( a >= 0n ) ? a : -a;
}

/**
 * An iterative implementation of the extended euclidean algorithm or extended greatest common divisor algorithm.
 * Take positive integers a, b as input, and return a triple (g, x, y), such that ax + by = g = gcd(a, b).
 * @param {bigint} a 
 * @param {bigint} b
 * @returns {{g: bigint, x: bigint, y: bigint}} A triple (g, x, y), such that ax + by = g = gcd(a, b).
 */
function bigint_egcd( a, b ) {
    let x = 0n
    let y = 1n
    let u = 1n
    let v = 0n
  
    while (a !== 0n) {
      const q = b / a
      const r = b % a
      const m = x - (u * q)
      const n = y - (v * q)
      b = a
      a = r
      x = u
      y = v
      u = m
      v = n
    }
    return {
      g: b,
      x,
      y
    }
}

/**
 * Modular inverse
 * 
 * @param {bigint} a The number to find an inverse for 
 * @param {bigint} n The modulo
 * @returns {bigint} The inverse modulo n
 */
function bigint_modinv( a, n ) {
    const egcd = bigint_egcd( bigint_toZn(a, n), n );
    return bigint_toZn( egcd.x, n )
}

/**
 * 
 * Modular exponentiation b**e mod n. Currently using the right-to-left binary method
 * 
 * @param {bigint} b 
 * @param {bigint} e 
 * @param {bigint} n 
 * @returns {bigint}
 */
function bigint_modpow( b, e, n ) {
    if( n === 1n ) return 0n;

    b = bigint_toZn(b, n);

    if( e < 0n ) return bigint_modinv( bigint_modpow( b, bigint_abs(e), n ), n );

    let r = 1n;
    while (e > 0) {
        if ((e % 2n) === 1n) {
            r = r * b % n
        }
        e = e / 2n
        b = b ** 2n % n
    }
    return r
}


/**
 * 
 * @param {bigint} e 
 * @param {bigint} n
 * @returns {bigint} 
 */
function g_modpow( e, n ) {
    return bigint_modpow( Generator, e, n );
}


// const SRPClient = require("secure-remote-password/client");

/**
 * 
 * @param {string} username 
 * @param {string} password 
 * @param {Buffer} salt Little Endian Array of 32 Bytes
 * @returns { Buffer } Little Endian Array of 32 Bytes
 */
function calculate_password_verifier(username, password, salt)
{
    const x = calculate_x( username, password, salt );
    return bigint_to_lebuffer( g_modpow( lebuffer_to_bigint(x), LargeSafePrime ) );
    // return Generator ** x % LargeSafePrime;
    // return modularPowerBigInt( Generator, x, LargeSafePrime )
}

/**
 * 
 * @param {string} username 
 * @param {string} password 
 * @param {Buffer} salt little endian array of 32 bytes
 * @returns { Buffer } little endian array of 20 bytes
 */
function calculate_x(username, password, salt)
{
    const interim = crypto.createHash( HashType )
        .update( username + ":" + password )
        .digest();

    return crypto.createHash( HashType )
        .update( salt )
        .update( interim )
        .digest();
}

/**
 * 
 * @param {Buffer} lebuffer Little Endian Buffer
 * @returns { bigint } 
 */
function lebuffer_to_bigint(lebuffer) {
    return BigInt( "0x" + lebuffer.reverse().toString("hex") );
}

/**
 * 
 * @param {bigint} bigint
 * @returns { Buffer } 
 */
function bigint_to_lebuffer( bigint )
{
    return Buffer.from( (bigint).toString(16), "hex" ).reverse();
}

// /**
//  * 
//  * @param {bigint} bigint
//  * @returns { Buffer } 
//  */
//  function bigint_to_bebuffer( bigint )
//  {
//      return Buffer.from( (bigint).toString(16), "hex" );
//  }

/**
 * 
 * @param {Buffer} password_verifier little endian array of 32 bytes
 * @param {Buffer} server_private_key little endian array of 32 bytes
 * @returns {Buffer} little endian array of 32 bytes
 */
function calculate_server_public_key( password_verifier, server_private_key ) 
{
    const interim = KValue * lebuffer_to_bigint( password_verifier ) 
                    + g_modpow( lebuffer_to_bigint( server_private_key), LargeSafePrime );
    
    return Buffer.from(  (interim % LargeSafePrime).toString(16), "hex" ).reverse();
}


/**
 * 
 * @param {Buffer} client_private_key little endian array of 32 bytes
 * @param {Buffer} server_public_key little endian array of 32 bytes
 * @param {Buffer} x little endian array of 32 bytes
 * @param {Buffer} u little endian array of 32 bytes
 * @returns {Buffer} little endian array of 32 bytes
 */
function calculate_client_s_key( client_private_key, server_public_key, x, u )
{
    const B = lebuffer_to_bigint( server_public_key );
    const a = lebuffer_to_bigint( client_private_key );
    const X = lebuffer_to_bigint( x );
    const U = lebuffer_to_bigint( u );

    return bigint_to_lebuffer(
        bigint_modpow ( 
        B - (KValue * g_modpow( X, LargeSafePrime ))
        , a + U * X, LargeSafePrime)
    )
}

/**
 * 
 * @param {Buffer} client_public_key little endian array of 32 bytes
 * @param {Buffer} password_verifier little endian array of 32 bytes
 * @param {Buffer} u little endian array of 32 bytes
 * @param {Buffer} server_private_key little endian array of 32 bytes
 * @returns {Buffer} little endian array of 32 bytes
 */
function calculate_server_s_key( client_public_key, password_verifier, u, server_private_key ) {
    const A = lebuffer_to_bigint( client_public_key );
    const v = lebuffer_to_bigint( password_verifier );
    const U = lebuffer_to_bigint( u );
    const b = lebuffer_to_bigint( server_private_key );

    return bigint_to_lebuffer( 
        bigint_modpow(
            A * bigint_modpow( v, U, LargeSafePrime )
        ,   b, LargeSafePrime
        )
    )


}

/**
 * 
 * @param {Buffer} client_public_key little endian array of 32 bytes
 * @param {Buffer} server_public_key little endian array of 32 bytes
 * @returns {Buffer} little endian array of 20 bytes
 */
 function calculate_u(client_public_key, server_public_key)
 {
     return crypto.createHash( HashType )
     .update( client_public_key )
     .update( server_public_key )
     .digest()
 }



/**
 * 
 * @param {Buffer} s_key little endian array of 32 bytes
 * @returns {Buffer} session_key -> little endian array of 40 bytes
 */
function calculate_session_key(s_key) 
{
    /**
     *    session key
    The SHA_Interleave function used in SRP-SHA1 is used to generate a
    session key that is twice as long as the 160-bit output of SHA1.  To
    compute this function, remove all leading zero bytes from the input.
    If the length of the resulting string is odd, also remove the first
    byte.  Call the resulting string T.  Extract the even-numbered bytes
    into a string E and the odd-numbered bytes into a string F, i.e.
     */

    for( let idx = 0; idx < s_key.length; idx++ )
        if( s_key[idx] != 0 && ( s_key.length - idx ) % 2 == 0 )
            if( idx == 0 ) break;
            else {
                s_key = s_key.slice( idx );
                break;
            }

    const E = Buffer.alloc( s_key.length / 2 );
    const F = Buffer.alloc( s_key.length / 2 );
    for( let idx = 0; idx < s_key.length; idx ++ )
    {
        const newidx = Math.trunc( idx / 2 );
        if( idx % 2 == 0 ) E[ newidx ] = s_key[idx];
        else F[newidx] = s_key[idx];
    }
    const G = crypto.createHash(HashType).update( E ).digest();
    const H = crypto.createHash(HashType).update( F ).digest();
    const session_key = Buffer.alloc(40);
    for( let i = 0; i < 20; i++ ) {
        session_key[ i * 2 ] = G[i];
        session_key[ ( i * 2 ) + 1 ] = H[i]
    }
    return session_key;
}



/**
 * 
 * @param {Buffer} client_public_key little endian array of 32 bytes
 * @param {Buffer} client_proof little endian array of 20 bytes
 * @param {Buffer} session_key little endian array of 40 bytes
 * @returns {Buffer} little endian array of 20 bytes (SHA1 Hash)
 */
function calculate_server_proof( client_public_key, client_proof, session_key )
{
    return crypto.createHash( HashType )
            .update( client_public_key )
            .update( client_proof )
            .update( session_key )
            .digest();
}




/**
 * 
 * @param {bigint} generator 
 * @param {bigint} large_safe_prime 
 * @returns {Buffer} little endian array of 20 bytes
 */
function calculate_xor_hash( generator, large_safe_prime )
{
    // TODO - figure out why Buffer.from acts weird with small values like 7
    const generator_hashed = crypto.createHash( HashType ).update( Buffer.from([generator.toString()]) ).digest();
    const generator_lsp = crypto.createHash( HashType ).update( bigint_to_lebuffer( large_safe_prime ) ).digest();

    const xor_hash = Buffer.alloc(20);
    for( let idx = 0; idx < generator_hashed.length; idx ++ )
        xor_hash[idx] = generator_hashed[idx] ^ generator_lsp[idx];
    
    return xor_hash;
} 

// console.log( new Uint8Array(XOR_HASH)  ) 



/**
 * 
 * @param { Buffer } xor_hash little endian array of 20 bytes
 * @param { string } username 
 * @param { Buffer } session_key little endian array of 40 bytes
 * @param { Buffer } client_public_key little endian array of 32 bytes
 * @param { Buffer } server_public_key little endian array of 32 bytes
 * @param { Buffer } salt little endian array of 32 bytes
 * @returns { Buffer }
 */
function calculate_client_proof( username, session_key, client_public_key, server_public_key, salt )
{
    const username_hashed = crypto.createHash( HashType ).update( username ).digest();
    return crypto.createHash( HashType )
        .update( XOR_HASH )
        .update( username_hashed )
        .update( salt )
        .update( client_public_key )
        .update( server_public_key )
        .update( session_key )
        .digest()
}



/**
 * 
 * @param { Buffer } client_private_key little endian array of 32 bytes
 * @returns { Buffer } little endian array of 32 bytes
 */
function calculate_client_public_key( client_private_key )
{
    return bigint_to_lebuffer( g_modpow( lebuffer_to_bigint( client_private_key ), LargeSafePrime ));
}

/**
 * 
 * @param { string } username 
 * @param { Buffer } client_data little endian array of 16 bytes
 * @param { Buffer } server_data little endian array of 16 bytes
 * @param { Buffer } session_key little endian array of 40 bytes
 * @returns { Buffer } little endian array of 20 bytes
 */
function calculate_reconnect_proof( username, client_data, server_data, session_key )
{
    return crypto.createHash( HashType )
        .update( username )
        .update( client_data )
        .update( server_data )
        .update( session_key )
        .digest();
}




const HexBuff = (hex_str) => Buffer.from(hex_str, "hex");
const ReverseHexBuff = (hex_str) => HexBuff(hex_str).reverse();

// const XTestSalt = ReverseHexBuff("CAC94AF32D817BA64B13F18FDEDEF92AD4ED7EF7AB0E19E9F2AE13C828AEAF57");
// const XTestUser = "USERNAME123", XTestPass = "PASSWORD123";

/**
 * 
 * @param {Buffer} lebuffer
 * @returns {string} 
 */
function LEBufferToBEHexStr( lebuffer )
{
    return lebuffer.reverse().toString("hex").toUpperCase();
}

function LEBufferToLEHexStr( lebuffer )
{
    return lebuffer.toString("hex").toUpperCase();
}



console.log("calculate_password_verifier")
console.log(
    LEBufferToBEHexStr(  
    calculate_password_verifier( "LF2BGFQIFQ3HZ1ZF", "MVRVMUJFWRA0IBVK"
        , ReverseHexBuff("AFE5D28E925DBB3DAFED5D91ACA0928940E8FBFEF2D2A3CC154ADA0FE6ABEF6F")
    )) == "21B4153B0A938D0A69D28F2690CC3F79A99A13C40CACB525B3B79D4201EB33FF"
)

console.log("calculate_server_public_key")
console.log(
    LEBufferToBEHexStr(
    calculate_server_public_key(
        ReverseHexBuff("870A98A3DA8CCAFE6B2F4B0C43A022A0C6CEF4374BA4A50CEBF3FACA60237DC4"),
        ReverseHexBuff("ACDCB7CB1DE67DB1D5E0A37DAE80068BCCE062AE0EDA0CBEADF560BCDAE6D6B9")
    )) == "85A204C987B68764FA69C523E32B940D1E1822B9E0F134FDC5086B1408A2BB43"
)

console.log("calculate_client_s_key")
console.log(
    LEBufferToBEHexStr(
    calculate_client_s_key( 
        ReverseHexBuff("FC3D610C4E2CEC5ECC7E47344D0ED81D2ACB938AB198EC7E2ED474AEFCC3ABD1")
    ,   ReverseHexBuff("E232D2C71AD1BF58DB9F7DBE51FFE271B6BDC61524F2E6B32ABFFFCAB09D09AB")
    ,   ReverseHexBuff("A4A7CB7DFBE00D26EE06F6B3DACC51E5779D7E8B")
    ,   ReverseHexBuff("FDAFAEF0E77F0FE1BD2956CF1820D4BC964E5283")
    )) == "3898DF5193EA6AA8111524A253DB480A51EA6160D1E41BC4B662420299B4A435"
)

console.log("calculate_server_s_key")
console.log(
    LEBufferToBEHexStr(
    calculate_server_s_key(
        ReverseHexBuff("51CCDDFACF7F960EDF5030F09F0B033C0D08DB1E43FCBA3A92ABB4BE3535D1DB")
    ,   ReverseHexBuff("6FC7D4ACFCFFFDCF780EE9BBD17AE507FFCDF586F83B2C9AEE2198F195DB3AB5")
    ,   ReverseHexBuff("F9CEDDD82E776BEDB1A94852A9A7FFA4FCADD5DE")
    ,   ReverseHexBuff("A5DBBFCB4C7A1B7C3041CAC9DDBD36CD646F9FBABDAD66A019BCBB8FEDF2FAAE")
    )) == "3503B289A60D6DD59EBD6FD88DF24836833433E39048ECAFF7E887313554F85C"
)



console.log("calculate_u")
console.log(
    LEBufferToBEHexStr(
        calculate_u(
        ReverseHexBuff("6FCEEEE7D40AAF0C7A08DFE1EFD3FCE80A152AA436CECB77FC06DAF9E9E5BDF3")
    ,   ReverseHexBuff("F8CD769BDE603FC8F48B9BE7C5BEAAA7BD597ABDBDAC1AEFCACF0EE13443A3B9")
    )) == "1309BD7851A1A505B95D6F60A8D884133458D24E"
)



console.log("calculate_session_key")
console.log(
    LEBufferToLEHexStr(
        calculate_session_key(
        HexBuff("8F4CEBD60DFC34E5C007E51BD4F3A4FF2BC1D930E2D3EA770D8D3EEDFF2DCCFC")
    )) == "EE144E1AE08DAC891AB63ABC42BF89738003343422E6B58131BEE4C3087A7027E55A7216D18D556C"
)



console.log("calculate_server_proof")
console.log(
    LEBufferToBEHexStr(
        calculate_server_proof(
        ReverseHexBuff("BFD1AC65C8DAAAD88BF9DFF9AF8D1DCDF11DFD0C7E398EDCDF5DBBD08EFB39D3")
    ,   ReverseHexBuff("7EBBC190D9AB2DC0CD891372CB30DF1ED35CDA1E")
    ,   HexBuff("9382b5e82c16e1105b8e8e88a99118811d88170fad6e8b35f236dbebbcc9c99bcab6cc9f8fe67648")
    )) == "269E3A3EF5DCD15944F043513BDA20D20FEBA2E0"
)



console.log("calculate_client_proof")
console.log(
    LEBufferToBEHexStr(
        calculate_client_proof(
        "7WG6SHZL33JMGPO4"
    ,   HexBuff("77a4d39cf9c0bf373ef870bd2941c339c575fdd1cbaa31c919ea7bd5023267d303e20fec9a9c402f")
    ,   ReverseHexBuff("0095FE039AFE5E1BADE9AC0CAEC3CB73D2D08BBF4CA8ADDBCDF0CE709ED5103F")
    ,   ReverseHexBuff("00B0C41F58CCE894CFB816FA72CA344C9FE2ED7CE799452ADBA7ABDCD26EAE75")
    ,   ReverseHexBuff("00a4a09e0b5aca438b8cd837d0816ca26043dbd1eaef138eef72dcf3f696d03d")
    )) == "7D07022B4064CCE633D679F61C6B212B6F8BC5C3"
)

console.log("calculate_client_public_key")
console.log(
    LEBufferToBEHexStr(
        calculate_client_public_key(
        ReverseHexBuff("A47DD4CD70DA1B0EF7E1FA8C02DE68AF0CEFCC77ACA287FBC3ADCDE0E7B78FE7")
    )) == "7186DF27C1A309B5B26E293CD00ADD01E7037E09116089F26E810FD2D962BC42"
)


console.log("calculate_reconnect_proof")
console.log(
    LEBufferToLEHexStr(
        calculate_reconnect_proof(
        "GXJ8M6VDUAC0JL9W"
    ,   HexBuff("DD801B2FBCF4F7ABC6023EFAAF6A9AEA")
    ,   HexBuff("0D27763BDEEF92CB273B7BC4EE72D0EC")
    ,   HexBuff("6A0E7B35C70C70DA142D57BF49FD25D84CCEE3D21CC1A10AD71323FB34F45F3006D606F1F39A6BB9")
    )) == "D94CE2B08B7FAC0919D7D5419D78CABFA372B6A9"
)


var SRPServer = {
    /**@type{Record<string, {encrypt_idx: number, encrypt_last_value: number}>} */
    sessions: {}
}


/**
 * 
 * @param {Buffer} data little endian array of length AL
 * @param {Buffer} session_key 
 * @returns {Buffer} little endian array of length AL
 */
function session_key_encrypt( data, session_key )
{
    const session_hex = session_key.toString("hex")
    let idx = SRPServer.sessions?.[session_hex]?.encrypt_idx ?? 0;
    let last_val = SRPServer.sessions?.[session_hex]?.encrypt_last_value ?? 0;

    const ret = Buffer.alloc( data.length );
    let idx_ret = 0;
    /**@type{number} */
    let encrypted;
    for( let unencrypted of data ) {
        encrypted = ( unencrypted ^ session_key[idx] ) + last_val;

        idx = ( idx + 1 ) % session_key.length; // Use the session key as a circular buffer

        ret[idx_ret++] = encrypted;
        last_val = encrypted;
    }

    if( SRPServer.sessions[session_hex] == null ) SRPServer.sessions[session_hex] = {}; 
    SRPServer.sessions[session_hex].encrypt_idx = idx;
    SRPServer.sessions[session_hex].encrypt_last_value = last_val;

    return ret;
}


console.log("session_key_encrypt")
console.log(
    LEBufferToLEHexStr(
        session_key_encrypt(
        HexBuff("3d9ae196ef4f5be4df9ea8b9f4dd95fe68fe58b653cf1c2dbeaa0be167db9b27df32fd230f2eab9bd7e9b2f3fbf335d381ca")
    ,   HexBuff("2EFEE7B0C177EBBDFF6676C56EFC2339BE9CAD14BF8B54BB5A86FBF81F6D424AA23CC9A3149FB175")
    )) == "13777da3d109b912322a08841e3ff5bc92f4e98b77bb03997da999b22ae0b926a3b1e56580314b3932499ee11b9f7deb6915".toUpperCase()
)


/**
 * 
 * @param {Buffer} data little endian array of length AL
 * @param {Buffer} session_key 
 * @returns {Buffer} little endian array of length AL
 */
 function session_key_decrypt( data, session_key )
 {
     const session_hex = session_key.toString("hex")
     let idx = SRPServer.sessions?.[session_hex]?.encrypt_idx ?? 0;
     let last_val = SRPServer.sessions?.[session_hex]?.encrypt_last_value ?? 0;
 
     const ret = Buffer.alloc( data.length );
     let idx_ret = 0;
     /**@type{number} */
     let unencrypted;
     for( let encrypted of data ) {
         unencrypted = ( encrypted - last_val) ^ session_key[idx];
 
         idx = ( idx + 1 ) % session_key.length; // Use the session key as a circular buffer
 
         ret[idx_ret++] = unencrypted;
         last_val = encrypted;
     }
 
     if( SRPServer.sessions[session_hex] == null ) SRPServer.sessions[session_hex] = {}; 
     SRPServer.sessions[session_hex].encrypt_idx = idx;
     SRPServer.sessions[session_hex].encrypt_last_value = last_val;
 
     return ret;
 }
 

console.log("session_key_decrypt")
console.log(
    LEBufferToLEHexStr(
        session_key_decrypt(
        HexBuff("84cdaae7cd6bf4e8214a8eeefe8eb8969a732806edab7d01dabf6af768daa4c90b90b698df1a8a9a02fd8a14a1bfef02a6fb")
    ,   HexBuff("C64EF330CB0C3B7E1C3C4A56DC4CCC257704E60C8CC2B22BC6FADB3D5A6ECFBE36FEAB9FFFF9C64B")
    )) == "42072e0d2d92b28a25150e36ccdce6fb73dd53d26b7c60af1f1f70b02b1c059b747b8d7db8c2b65baeb57eba46120b6db869".toUpperCase()
)


/**
 * 
 * @param { string } username string
 * @param { Buffer } client_seed unsigned 4 byte value, converted into little endian bytes
 * @param { Buffer } server_seed unsigned 4 byte value, converted into little endian bytes
 * @param { Buffer } session_key little endian array of 40 bytes
 * @returns { Buffer } little endian array of 20 bytes
 */
function calculate_world_server_proof( username, client_seed, server_seed, session_key )
{
    const zero = Buffer.from([0,0,0,0])

    return crypto.createHash( HashType )
        .update( username )
        .update( zero )
        .update( client_seed )
        .update( server_seed )
        .update( session_key )
        .digest()
}


console.log("calculate_world_server_proof")
console.log(
    LEBufferToLEHexStr(
        calculate_world_server_proof(
        "HQO7EWULX09Z4RE4", HexBuff("a2ba5fb2"), HexBuff("2d0a01e2")
    ,   ReverseHexBuff("77295B4E6745E8833293E07650252D635D5E4B14D2A9DA4FB1AE22FB00131E42C2B2EE7BF0D4D185")
    )) == "b26af9256f4bd20f0f11e2c786710542b92115bb".toUpperCase()
)

module.exports = {
    LargeSafePrimeLEB,
    calculate_server_public_key,
    calculate_password_verifier
}



// Small Endian Hex
// console.log( calculate_x( XTestUser, XTestPass, XTestSalt ) )
// Big Endian Hex
// console.log( calculate_x( XTestUser, XTestPass, XTestSalt ).reverse() ) // D9 27 E9 8B E3E9AF84FDC99DE9034F8E70ED7E90D6
// console.log( calculate_x_bigint( XTestUser, XTestPass, XTestSalt ) )
// console.log( calculate_x_bigint( XTestUser, XTestPass, XTestSalt ).toString(16) )



// console.log( 
//     calculate_x("USERNAME123", "PASSWORD123"
//     , Buffer.from("CAC94AF32D817BA64B13F18FDEDEF92AD4ED7EF7AB0E19E9F2AE13C828AEAF57", "hex").reverse()  
//     ).reverse() ) 
    
// console.log( calculate_x_bigint( "LF2BGFQIFQ3HZ1ZF", "MVRVMUJFWRA0IBVK"
// , Buffer.from("AFE5D28E925DBB3DAFED5D91ACA0928940E8FBFEF2D2A3CC154ADA0FE6ABEF6F", "hex").reverse() ) )


// console.